![Terraform](https://lgallardo.com/images/terraform.jpg)

# TF-AWS-CodeCommit
---
This module manage AWS Security Token Service [terraform-module-aws-codecommit].

## MODULE

### SIMPLE
```hcl
# ws@20xx

# #################################################################
# Create repository: "${var.cut_name}-<repo_name>"
# #################################################################

module "codecommit_<repo_name>" {

  #source    = "git::<url>?ref=<tag>"
  source     = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/devtools/tf-aws-codecommit.git?ref=tags/1.0.x"
  #source    = "./modules/codecommit_<repo_name>"

  # #################################################################
  # Common/General settings
  # #################################################################

  # Whether to create the resources (`false` prevents the module from creating any resources).
  module_enabled = false

  # The name for the repository.
  repository_name = "${var.cut_name}-<repo_name>"

  # The description of the repository
  repository_description = "Git Repository in AWS ${var.cut_name}-<repo_name>"

  # The default branch of the repository
  default_branch = "master"

  # kms
  #kms_master_key_id = ""

  ## ################################################################
  ## SNS
  ## ################################################################
  
  # Whether to create the resources (`false` prevents the module from creating any resources).
  sns_enabled = false

  ## ################################################################
  ## CloudWatch
  ## ################################################################
  
  # Whether to create the resources (`false` prevents the module from creating any resources).
  cloudwatch_enabled = false

  ## ################################################################
  ## Other settings
  ## ################################################################

  # Emulation of `depends_on` behavior for the module.
  # Non zero length string can be used to have current module wait for the specified resource.
  module_depends_on = ""

  # Additional mapping of tags to assign to the all linked resources.
  module_tags = {
    ManagedBy   = "Terraform"
  }

}

```

### COMPLETE

```hcl
# ws@20xx
# Create repository: "${var.cut_name}-<repo_name>"

module "codecommit_name" {

  #source    = "git::<url>?ref=<tag>"
  source     = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/devtools/tf-aws-codecommit.git?ref=tags/1.0.x"
  #source    = "./modules/codecommit_<repo_name>"

  ##################################################################
  # Common/General settings
  ##################################################################

  # Whether to create the resources (`false` prevents the module from creating any resources).
  module_enabled = false

  # The name for the repository.
  repository_name = "${var.cut_name}-<repo_name>"

  # The description of the repository
  repository_description = ""Git Repository in AWS <repo_name> .."

  # The default branch of the repository
  default_branch = "master"

  # Triggers
  triggers = [
    {
      name            = "all"
      events          = ["all"]
      destination_arn = "arn:aws:lambda:us-east-1:12345678910:function:lambda-all"
    },
    {
      name            = "updateReference"
      events          = ["updateReference"]
      destination_arn = "arn:aws:lambda:us-east-1:12345678910:function:lambda-updateReference"
    },
  ]

  # Emulation of `depends_on` behavior for the module.
  # Non zero length string can be used to have current module wait for the specified resource.
  module_depends_on = ""

  # Additional mapping of tags to assign to the all linked resources.
  module_tags = {
    ManagedBy   = "Terraform"
  }
}
```

```sh
$>terraform apply
$>git clone https://..
$>cd xx
$>echo 'hello world' > touch.txt
$>git commit -a -m 'init master'
$>git push -u origin master(main)
```



## Instructions

This modules creates a repo with direct updates to the master denied. 

To use this repository the expected behaviour is to branch when starting a new piece of work, for example.

`git pull`

`git branch -b feature/..`

Do your work, check-in.
Push to your feature branch.

`git push -u origin feature/..`

Then when your done create a PR and request the merge.

## Details

Creates a group called developer, to which the policy is attached.
To use the repo you need to add the your users to that group.

## Using Codecommit

To use codecommit you need to set some git config properties for the credential helper:

`git config --global credential.helper '!aws codecommit credential-helper $@'`

`git config --global credential.UseHttpPath true`

And for SSH look at: <https://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-ssh-unixes.html>

Use ssh-keygen and create a key in your home folder called codecommit

`publickey=$(<~/.ssh/codecommit.pub)`

`user=$(aws iam get-user --query 'User.UserName' --output text)`

### On Windows

`$publickey=get-content ~/.ssh/codecommit.pub`

`$user=aws iam get-user --query 'User.UserName'`

On both:
`aws iam upload-ssh-public-key --user-name $user --ssh-public-key-body $publickey`

Get your ssh key id from the previous commands output

`SSHPublicKeyId=$(aws iam list-ssh-public-keys --user-name $user --query 'SSHPublicKeys[*].SSHPublicKeyId' --output text)`

OR

`$SSHPublicKeyId=(aws iam list-ssh-public-keys --user-name $user --query 'SSHPublicKeys[*].SSHPublicKeyId')|convertfrom-json`

Update your config file with:

```sh
$gitconfigupdate=@"
Host git-codecommit.\*.amazonaws.com
User $SSHPublicKeyId
IdentityFile ~/.ssh/codecommit
"@
Add-content ~/.ssh/config \$gitconfigupdate
```

### linux

```sh
cat << EOF > ~/.ssh/config
Host git-codecommit.\*.amazonaws.com
User \$SSHPublicKeyId
IdentityFile ~/.ssh/codecommit
EOF
```

```sh
eval \$(ssh-agent -s)
ssh-add codecommit
```

Test with:
```sh
`ssh git-codecommit.us-east-2.amazonaws.com`
```

Please note that using the above snippet, the generated access key and secret access key will be available as plain text in the state file, whether that is local or remote. 
There are ways to encrypt them with using a pgp key, but that goes beyond the scope of this post.

Finally, we can use the local-exec provisioner to sync files to the remote Git repository. 
Assuming the files are available within a local folder called repo-init, the code that would sync the files can be found below.

```json
resource "null_resource" "git_clone" {
  provisioner "local-exec" {
    command = <<EOT
      export AWS_ACCESS_KEY_ID=${aws_iam_access_key.pipeline_codecommit_user.id}
      export AWS_SECRET_ACCESS_KEY=${aws_iam_access_key.pipeline_codecommit_user.secret}
      git config --global credential.helper '!aws codecommit credential-helper $@'
      git config --global credential.UseHttpPath true
      git clone ${aws_codecommit_repository.repository.clone_url_http} ../temp-location
      cp -r repo-init/**/* ../temp-location
      cd ../temp-location
      git add . && git commit -am "Initial commit" && git push origin main
      rm -rf ../temp-location
    EOT
  }
}
```


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5 |
| <a name="provider_random"></a> [random](#provider\_random) | >= 2.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|


## Resources

| Name | Type |
|------|------|
| [aws_codecommit_repository.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codecommit) | resource |
| [locals](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/locals) | resource |
| [aws_codecommit_trigger.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codecommit_trigger) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="module_enable"></a> [module\_enable](#input\_module\_enable) | Whether to create the resources | `bool` | `false` | true |
| <a name="repository_name"></a> [repository\_name](#input\_repository\_name) | The name for the repository | `string` | n/a | yes |
| <a name="default_branch"></a> [default\_branch](#input\_default\_branch) | The default branch of the repository | `string` | n/a | yes |
| <a name="repository_description"></a> [repository\_description](#input\_repository\_description) | The description of the repository | `string` | n/a | yes |
| <a name="triggers"></a> [triggers](#input\_triggers) | List of triggers | `any` | `[]` | no |
| <a name="module_depend_on"></a> [module\_depend\_on](#input\_module\_depend\_on) | Emulation of behavior for the module. | `any` | `""` | no |
| <a name="name"></a> [name](#input\_name) | Application name | `string` | `""` | name |
| <a name="module_tags"></a> [module\_tags](#input\_module\_tags) | A mapping of tags to assign to the resource | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="repository_name"></a> [repository\_name](#output\_repository\_name) | The name for the repository |
| <a name="repository_arn"></a> [repository\_arn](#output\_repository\_arn) | The ARN of the repository|
| <a name="repository_id"></a> [repository\_id](#output\_repository\_id) | The ID of the repository |
| <a name="repository_clone_url_http"></a> [repository\_clone\_url\_http](#output\_repository\_clone\_url\_http) | The URL to use for cloning the repository over HTTPS |
| <a name="repository_clone_url_ssh"></a> [repository\_clone\_url\_ssh](#output\_repository\_clone\_url\_ssh) | The URL to use for cloning the repository over SSH |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Versioning
This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks
This repository uses [pre-commit](https://pre-commit.com/) hooks.

## URLs

1. https://github.com/lgallard/terraform-aws-codecommit/
2. https://github.com/JamesWoolfenden/terraform-aws-codecommit


##


