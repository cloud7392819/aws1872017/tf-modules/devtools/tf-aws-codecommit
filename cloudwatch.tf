# ws@2023

# ###############################################################
# CloudWatch
# ################################################################

## ################################################################
## CloudWatch Event Target: Default
## ################################################################

resource "aws_cloudwatch_event_rule" "default" {

  count = (local.create && var.cloudwatch_enabled) ? 1 : 0 

  description = "An Amazon CloudWatch Event rule has been created by AWS CodeCommit for the following repository: ${join("", aws_codecommit_repository.default.*.arn)}."

  #is_enabled = true

  event_pattern = <<PATTERN
{
  "source": [
    "aws.codecommit"
  ],
  "resources": [
    "${join("", aws_codecommit_repository.default.*.arn)}"
  ],
  "detail-type": [
    "CodeCommit Pull Request State Change",
    "CodeCommit Comment on Pull Request",
    "CodeCommit Comment on Commit"
  ]
}
PATTERN
}

## ################################################################
## CloudWatch Event Target: Default
## ################################################################

resource "aws_cloudwatch_event_target" "default" {

  count = (local.create && var.cloudwatch_enabled) ? 1 : 0 

  target_id = "codecommit_notification"

  rule      = join("", aws_cloudwatch_event_rule.default.*.name)
  
  arn       = join("", aws_sns_topic.default.*.arn)

  input_path = "$.detail.notificationBody"
}





