# ws@2023 sns.tf

# #################################################################
# SNS
# #################################################################

## ################################################################
## SNS TOPIC POLICY: Default
## ################################################################

resource "aws_sns_topic" "default" {

  count = (local.create && var.sns_enabled) ? 1 : 0 

  name  = "codecommit_${replace(local.repository_name, ".", "_")}"
  
  kms_master_key_id = local.kms_master_key_id
}

## ################################################################
## SNS TOPIC POLICY: Default
## ################################################################

resource "aws_sns_topic_policy" "default" {

  count = (local.create && var.sns_enabled) ? 1 : 0 

  arn    = join("", aws_sns_topic.default.*.arn)

  policy = join("", data.aws_iam_policy_document.sns_topic_policy[*].json)

}

## ################################################################
## DATA IAM POLICY DOCUMENT: SNS TOPIC POLICY
## ################################################################

data "aws_iam_policy_document" "sns_topic_policy" {

  count = (local.create && var.sns_enabled) ? 1 : 0  

  statement {
    effect = "Allow"

    actions = [
      "SNS:GetTopicAttributes",
      "SNS:SetTopicAttributes",
      "SNS:AddPermission",
      "SNS:RemovePermission",
      "SNS:DeleteTopic",
      "SNS:Subscribe",
      "SNS:ListSubscriptionsByTopic",
      "SNS:Publish",
      "SNS:Receive",
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [join("", aws_sns_topic.default.*.arn)]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        join("", data.aws_caller_identity.default.*.account_id),
      ]
    }
  }
}

