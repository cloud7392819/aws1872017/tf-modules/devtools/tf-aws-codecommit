# ws@2023 variables.tf

## ################################################################
## Common/General settings
## ################################################################

variable "create" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

variable "module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

# Application name
variable "name" {
	description = "Application name"
	default		= null
}

variable "module_description" {
  description = "The description of the module. This needs to be less than 1000 characters"
  type        = string
  default     = null
}

variable "module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default     = ""
}

variable "module_tags" {
  description = "Additional mapping of tags to assign to the all linked resources."
  type        = map(string)
  default     = {}
}

# Triggers
variable "triggers" {
  description = "List of triggers"
  type        = any
  default     = []
}

## ################################################################
## REPOSITORY
## ################################################################

variable "repository_name" {
  description = "The name for the repository. This needs to be less than 100 characters."
  type        = string
  default     = null
}

variable "repository_description" {
  description = "The description of the repository. This needs to be less than 1000 characters"
  type        = string
  default     = null
}

variable "default_branch" {
  description = "The default branch of the repository. The branch specified here needs to exist."
  type        = string
  default     = null
}

## ################################################################
## SNS
## ################################################################

variable "sns_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

variable "kms_master_key_id" {
  type        = string
  description = "The kms key to use"
  default     = null
}

## ################################################################
## CloudWatch
## ################################################################

variable "cloudwatch_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

##################################################################
## 
##################################################################

variable "developer_group" {
  description = "An existing Iam Group to attach the policy permissions to"
  type        = string
  default     = null
}

variable "approver_role" {
  type        = string
  description = "ARN of approver role"
  default     = null
}

variable "template" {
  default = {
    name        = "MyExampleApprovalRuleTemplate"
    description = "This is an example approval rule template"
    approvers   = 2
  }
}
