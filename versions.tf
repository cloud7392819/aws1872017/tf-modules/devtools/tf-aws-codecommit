# ws@2023 versions.tf

terraform {
  
  required_version = ">= 1.0"

  required_providers {
    aws    = ">= 2.0"
  }

}
