# ws@2023

# #################################################################
# DATA
# #################################################################

data "aws_caller_identity" "default" {

  count = local.create ? 1 : 0  
}

## ################################################################
## IAM POLICY DOCUMENT: RESTRICTMASTER
## ################################################################

data "aws_iam_policy_document" "restrictmaster" {

  count = (local.create && var.sns_enabled) ? 1 : 0   

  statement {
    effect = "Allow"

    actions = [
      "codecommit:*",
    ]

    resources = [ join("", aws_codecommit_repository.default.*.arn) ]
  }

  statement {
    effect = "Deny"

    actions = [
      "codecommit:GitPush",
      "codecommit:DeleteBranch",
      "codecommit:PutFile",
      "codecommit:MergePullRequestByFastForward",
    ]

    resources = [ join("", aws_codecommit_repository.default.*.arn) ]

    condition {
      test     = "StringEqualsIfExists"
      variable = "codecommit:References"
      values   = ["refs/heads/${var.default_branch}", "refs/heads/prod"]
    }

    condition {
      test     = "Null"
      variable = "codecommit:References"
      values   = [false]
    }
  }
}

## ################################################################
## IAM GROUP POLICY ATTACHMENT: RESTRICTMASTER-ATTACH
## ################################################################

# resource "aws_iam_group_policy_attachment" "restrict-attach" {
#   count      = var.developer_group == "" ? 0 : 1
#   group      = var.developer_group
#   policy_arn = aws_iam_policy.restrictmaster.0.arn
# }