# ws@2023 outputs.tf

output "repository_name" {
  description = "The name for the repository"
  value       = var.repository_name
}

output "repository_id" {
  description = "The ID of the repository"
  value       = join ("", aws_codecommit_repository.default.*.repository_id)
}

output "repository_arn" {
  description = "The ARN of the repository"
  value       = join ("", aws_codecommit_repository.default.*.arn)
}

output "repository_clone_url_http" {
  description = "The URL to use for cloning the repository over HTTPS."
  value       = join ("", aws_codecommit_repository.default.*.clone_url_http)
}

output "repository_clone_url_ssh" {
  description = "The URL to use for cloning the repository over SSH."
  value       = join ("", aws_codecommit_repository.default.*.clone_url_ssh)
}




