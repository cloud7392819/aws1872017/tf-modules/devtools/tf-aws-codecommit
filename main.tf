# ws@2023 main.tf

# #################################################################
# CodeCommit: Default
# #################################################################

resource "aws_codecommit_repository" "default" {

  count = local.create ? 1 : 0  

  # The name for the repository.
  repository_name = local.repository_name

  # The description of the repository
  description     = local.repository_description
  
  # The default branch of the repository
  default_branch  = local.default_branch

  lifecycle {
    ignore_changes = all
  }

  #lifecycle {
  #  ignore_changes = [
  #    default_branch 
  #  ]
  #}

  # Tags
  tags = merge(
    local.tags,
    {
      RepoName  = local.repository_name
      Branch    = local.default_branch 
    }
  )

}


##################################################################
# CodeStarNotifications Rule: Default
##################################################################

resource "aws_codestarnotifications_notification_rule" "default" {

  count = (local.create && var.sns_enabled) ? 1 : 0 

  name           = "${local.repository_name}-notication-rule"

  detail_type    = "FULL"

  event_type_ids = [
    "codecommit-repository-approvals-rule-override",
    "codecommit-repository-approvals-status-changed",
    "codecommit-repository-branches-and-tags-created",
    "codecommit-repository-branches-and-tags-deleted",
    "codecommit-repository-branches-and-tags-updated",
    "codecommit-repository-comments-on-pull-requests",
    "codecommit-repository-pull-request-created",
    "codecommit-repository-pull-request-merged",
    "codecommit-repository-pull-request-source-updated",
    "codecommit-repository-pull-request-status-changed",
  ]

  resource  = join("", aws_codecommit_repository.default.*.arn)

  target {
    address = join("", aws_sns_topic.default.*.arn)
  }
}



###########################################################

## aws_codecommit_approval_rule_template.example.tf

# resource "aws_codecommit_approval_rule_template" "example" {

#   name        = var.template.name
#   description = var.template.description

#   content = <<EOF
# {
#     "Version": "2018-11-08",
#     "DestinationReferences": ["refs/heads/${local.default_branch}"],
#     "Statements": [{
#         "Type": "Approvers",
#         "NumberOfApprovalsNeeded": 2,
#         "ApprovalPoolMembers": ["${var.approver_role}"]
#     }]
# }
# EOF
# }

# resource "aws_codecommit_approval_rule_template_association" "link" {

#   approval_rule_template_name = aws_codecommit_approval_rule_template.example.name
#   repository_name             = aws_codecommit_repository.default.repository_name
# }

