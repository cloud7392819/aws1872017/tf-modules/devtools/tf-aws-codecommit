# ws@2023 locals.tf

locals {

  # create
  enabled = var.module_enabled ? true : false
  create  = var.module_enabled ? true : false

  # Repository
  repository_name         = var.repository_name
  repository_description  = var.repository_description
  default_branch          = var.default_branch


  # KMS [kms_master_key_id]
  kms_master_key_id = var.kms_master_key_id
    
  tags = merge(
    var.module_tags,
      {
        Terraform = true
      }
  )

}


