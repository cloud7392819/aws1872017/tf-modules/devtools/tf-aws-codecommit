# Triggers
resource "aws_codecommit_trigger" "default" {
  count           = length(var.triggers)
  repository_name = aws_codecommit_repository.default[*].repository_name

  trigger {
    name            = lookup(element(var.triggers, count.index), "name")
    events          = lookup(element(var.triggers, count.index), "events")
    destination_arn = lookup(element(var.triggers, count.index), "destination_arn")
  }
}
